Projeto de Gerenciamento de Threads
===========
> Objetivo
- Simulador para gerenciar memória em um sistema operacional. 

> Requisitos 
Esse gerenciador deverá conter um vetor para representar uma tabela de páginas. Além disso, o simulador deverá executar múltiplas threads. Como as múltiplas threads pertencem ao mesmo processo, o espaço de endereçamento na memória será compartilhada entre elas, ou seja, o vetor de tabela de página será um recurso compartilhado entre as threads. Portanto, ele deve ser gerenciado a fim de evitar condições de corrida. A execução do simulador deve ser realizada a partir da leitura de um arquivo de entrada do tipo txt, por exemplo:

```
  4
  2
  0 1 2 3 4 0 5 5 6 3
  3 0 2 2 1 5 0 3 2 1
```

- A primeira linha, há o número de páginas do seu gerenciador de memória. 
- A segunda linha contém a quantidade de threads que deve ser criada pelo seu simulador. 
- As demais linhas representam as páginas acessadas por cada thread (ex.: linha 3 – thread-1, linha 4 – thread-2). 
- A execução de cada thread deve ser armazenada em um arquivo de saída. Por exemplo, se a thread-1 deseja acessar a página 5 e esta não está na memória (vetor de tabela de páginas), a thread imprime no seu arquivo de saída, exatamente, a string “5-fault”. 
- Caso a página 5 esteja na memória, ainda que previamente carregada por outra thread, então, deve ser escrito no arquivo de saída “5-mem”.

```
Entrada: input.txt
Saída: arquivos com resultado da execução para cada thread com os nomes: thread-1.txt, thread-2.txt, ...

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void *funcao_comum( void *ptr );

int shared_mem = -1;

int
main(int argc, char **argv){
	pthread_t thread1, thread2;
	int mensagem1 = 1;
	int mensagem2 = 2;

	int  status_t1, status_t2;

	status_t2 = pthread_create( &thread2, NULL, funcao_comum, (void*) &mensagem2);
	status_t1 = pthread_create( &thread1, NULL, funcao_comum, (void*) &mensagem1);

	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL); 

	fprintf(stdout, "Status da execucao da Thread 1: %d\n",status_t1);
	fprintf(stdout, "Status da execucao da Thread 2: %d\n",status_t2);
	
	return EXIT_SUCCESS;
}

void *
funcao_comum( void *ptr ){
	int thread_id = *(int *) ptr;
	int novo_valor;

	srand(time(NULL));

	if(thread_id == 1){

		if(shared_mem == -1){
			fprintf(stdout, "Nenhum valor foi produzido... Thread %d passará sua vez\n", thread_id);
			pthread_yield(NULL);
		}


		fprintf(stdout, "Thread %d encontrou o valor: %d\n", thread_id, shared_mem);
	}else{
		shared_mem = rand()%20; 
		fprintf(stdout, "Thread %d produziu o valor: %d\n", thread_id, shared_mem);
	}

}

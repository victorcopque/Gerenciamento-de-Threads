#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void *funcao_comum(void *);
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int  contador = 0;

main()
{
   int status_thread1, status_thread2;
   pthread_t thread1, thread2;
   char *mensagem1 = "Thread 1";
   char *mensagem2 = "Thread 2";

   srand(time(NULL));

   status_thread1=pthread_create( &thread1, NULL, &funcao_comum, (void *) mensagem1);
   status_thread2=pthread_create( &thread2, NULL, &funcao_comum, (void *) mensagem2);

   pthread_join( thread1, NULL);
   pthread_join( thread2, NULL); 

   exit(0);
}

void *funcao_comum(void *vlr)
{
   printf("Executando thread %s \n", (char *)vlr);
   pthread_mutex_lock( &mutex );
   printf("Thread %s está na regiao critica\n", (char *)vlr);
   sleep(rand()%5);
   contador++;
   printf("Valor: %d\n",contador);
   pthread_mutex_unlock( &mutex );
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void *funcao_comum( void *ptr );

int shared_mem[2];
pthread_barrier_t barr;

int
main(int argc, char **argv){
	pthread_t thread1, thread2;
	int mensagem1 = 1;
	int mensagem2 = 2;

	int  status_t1, status_t2;

	srand(time(NULL));
	
	pthread_barrier_init(&barr, NULL, 2);

	shared_mem[0] = -1000;
	shared_mem[1] = -1000;

	status_t2 = pthread_create( &thread2, NULL, funcao_comum, (void*) &mensagem2);
	status_t1 = pthread_create( &thread1, NULL, funcao_comum, (void*) &mensagem1);

	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL); 

	printf("Status da execucao da Thread 1: %d\n",status_t1);
	printf("Status da execucao da Thread 2: %d\n",status_t2);
	
	return EXIT_SUCCESS;
}

void *
funcao_comum( void *ptr ){
	int thread_id = *(int *) ptr;

	shared_mem[thread_id-1] = rand()%10;
	pthread_barrier_wait(&barr);

	printf("Thread %d valor produzido %d -> total: %d\n", 
		thread_id, shared_mem[thread_id-1], shared_mem[0]+shared_mem[1]);

}

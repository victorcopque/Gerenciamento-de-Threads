#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void *funcao_comum( void *ptr );

int
main(int argc, char **argv){
	pthread_t thread1, thread2;
	char *mensagem1 = "Thread 1";
	char *mensagem2 = "Thread 2";

	int  status_t1, status_t2;

	srand(time(NULL));

	status_t1 = pthread_create( &thread1, NULL, funcao_comum, (void*) mensagem1);
	status_t2 = pthread_create( &thread2, NULL, funcao_comum, (void*) mensagem2);

	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL); 

	printf("Status da execucao da Thread 1: %d\n",status_t1);
	printf("Status da execucao da Thread 2: %d\n",status_t2);
	
	return EXIT_SUCCESS;
}

void *
funcao_comum( void *ptr ){
	int tempo = rand()%20;
	printf("A %s vai esperar por %d\n", (char *) ptr, tempo);
	sleep(tempo);
}

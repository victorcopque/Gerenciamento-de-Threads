#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>//necessário para strcat

void *funcao_principal(void *ptr);                 // 
void criation_threads();                          //
void get_number_pages_threads();                 //
void get_pages_threads();                       //Toda declaração de funções
void shared_memory_create();                   //
void creation_file(int x);

FILE *arquivo;//ponteiro do tipo arquivo
int leitura;//variável que percorre o arquivo
int number_pages=0;
int number_threads=0;
int controle=1;//variável apenas para controle de loop
int **paginas = NULL;
char *shared_memory = NULL;
int i;

int main(int argc, char **argv){

	get_number_pages_threads();	// 
	get_pages_threads();       //Quisemos deixar a main mais "limpa" apenas chamando outras funções
	shared_memory_create();   //
	criation_threads();      //

	return EXIT_SUCCESS;
}

void get_number_pages_threads(){
	arquivo = fopen("nome.txt","r");

	while((fscanf(arquivo,"%d",&leitura)) != EOF){//o fscanf vai percorrendo o arquivo mas
												 //nós restringimos para que apenas números
		if (controle==1){						//inteiros fossem lidos
			number_pages = leitura;
			shared_memory = (char*) malloc ((number_pages+1)*sizeof ( char ));//alocando espaço para o vetor do
			//o primeiro número que ele lê é a quantidade de Páginas          //tamanho lido no arquivo
		}

		if (controle==2){
			number_threads = leitura; //o segundo número que ele lê é a quantidade de Threads
			paginas = (int **)malloc(number_threads*sizeof(int*)); //Alocando espaço para a matriz
			for (i=0; i < number_threads; i++){                   //Alocando espaço para a matriz
     			paginas[i]=(int *)malloc(6*sizeof(int));         //Alocando espaço para a matriz
			}
		}

		if (controle > 2){break;}//Esse loop serve apenas para pegar essas duas variáveis.
								//Isso acontece porque posteriormente temos que declarar
							   //um vetor e uma matriz do tamanho que foi lido no arquivo. 
		controle++;           
	}

	controle=1;//a variável é reiniciada
	fclose(arquivo);
}

void get_pages_threads(){
	arquivo = fopen("nome.txt","r");//uma nova leitura exige uma nova abertura
	int linha_thread=0;//apenas para controle da matriz que guarda as páginas pedidas pelas threads

	while((fscanf(arquivo,"%d",&leitura)) != EOF){

		if (controle > 2){                          // 
			if (linha_thread==(number_threads)){   //Essa é apenas uma verificação para inserir dados na matriz
				linha_thread--;                   // 
			}                                    //
			
			paginas[linha_thread][controle-3]=leitura;//Adicionando o que foi lido na matriz
												     //Vale lembrar:cada thread tem sua linha e seis colunas
			if (controle==8){                 		//
				linha_thread++;                    //Essa é apenas uma verificação para inserir dados na matriz
				controle = 2;                     //
			} 
		}
		controle++;//contador de controle
	}
	fclose(arquivo);
}

void shared_memory_create(){
	for (i = 0; i < (number_pages+1); i++){ //
		shared_memory[i] = 'l';            //Deixando todos os epaços da memória compartilhada como "livres"
	}                                     //
}

void criation_threads(){
	pthread_t threads[number_threads];//vetor de threads
	for (i = 0; i < number_threads; i++){//cria as threads dinamicamente
    	pthread_create(&threads[i], NULL, funcao_principal,(void *)&i);
	}

	for (i = 0; i < number_threads; i++){
    	pthread_join(threads[i], NULL);
	}
}

void * funcao_principal(void *ptr){
	creation_file(*(int *) ptr);
}

void creation_file(int x){
	char formato[] = "thread_";
	char name_arquivo[] = "thread_";//esse vetor ira guardar o número da thread para concatenação
	char tipo[] = ".txt";

	sprintf(name_arquivo, "%d", x);  //guarda o inteiro como uma string dentro do vetor "name_arquivo"
	strcat(formato,name_arquivo);   //concatena o nome "thread_" com o número da thread. Ex: thread_0
	strcat(formato,tipo);		   //concatena o nome do arquivo com o tipo. Ex: thread_1.txt
	arquivo = fopen(formato,"a+");//Cria um arquivo com o nome especificado
	
	for (i = 0; i < 6; i++){
		if(shared_memory[paginas[x][i]] == 'l'){
			fprintf(arquivo, "%d - mem\n",paginas[x][i]);
			shared_memory[paginas[x][i]] = 'o';
		}else{
			fprintf(arquivo, "%d - fault\n",paginas[x][i]);
			shared_memory[paginas[x][i]] = 'f';
		}
	}

	fclose(arquivo);
}